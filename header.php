<!doctype html>
<html>
<head>
	<title><?php if($count >0){echo "(".$count.")";}; ?> Mindcrack Livestreams</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="Shortcut Icon" href="<?php echo ($count > 0 ? 'online' : 'offline'); ?>.png" type="image/x-icon" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-39403280-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body onload="javascript:setTimeout('location.reload(true);',300000);">
	<div class="announcement"><p>Update (20/05/2014): Aureylian added to list of streamers</p></div>
	<h1>Mindcrack</h1>
	<h2><?php echo $count; ?> Active Livestream<?php if($count != 1){echo "s";}?></h2>
	<table>
		<tr>
			<th>Name</th>
			<th>Playing</th>
			<th>Description</th>
			<th>Viewers</th>
		</tr>