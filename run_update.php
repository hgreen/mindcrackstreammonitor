<?php
header('Location: ./');

// Array of Mindcrackers and their twitch.tv usernames.
// "Pretty Name" => "twitchusername"
$streamers = array(
                   "Adlington" => "adlingtontplays",
                   "AnderZEL" => "anderzel",
                   "Arkas" => "mc_arkas",
                   "Aureylian" => "aureylian",
                   "Avidya" => "avidyazen",
                   "BdoubleO" => "bdoubleo",
                   "BlameTC" => "blamethecontroller",
                   "BlameTC (DenialTV)" => "denialtv",
                   "Docm" => "docm77live",
                   "Etho" => "ethotv",
                   "GenerikB" => "generikb",
                   "Guude" => "guude",
                   "Jsano" => "jsano19",
                   "Kurt" => "kurtjmac",
                   "MCGamer" => "supermcgamer",
                   "Mhykol" => "mhykol",
                   "Millbee" => "millbee",
                   "Nebris" => "nebris",
                   "Pakratt" => "pakratt0013",
                   "PaulSoaresJr" => "paulsoaresjr",
                   "Pause" => "pauseunpause",
                   "Pyrao" => "pyropuncher",
                   "SethBling" => "sethbling",
                   "TheJims" => "thejimslp",
                   "Vintage Beef" => "vintagebeef",
                   "W92Baj" => "w92baj",
                   "Zisteau" => "zisteau",
                   "Mindcrack Network" => "mindcracknetwork"
					// "Zeldathon" => "funforfreedom"
						 );

// Twitch helper function to read the contents of the json object
function get_url_contents($url){
    $crl = curl_init();
    $timeout = 5;
    curl_setopt ($crl, CURLOPT_URL,$url);
    curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
    $ret = curl_exec($crl);
    curl_close($crl);
    return $ret;
}

// Sends a Twitch API request to get stream status
function stream_lookup($streamers){
	$list = implode(",", $streamers);
	$url = "http://api.justin.tv/api/stream/list.json?channel=".$list;
	$json = get_url_contents($url);
	$json_output = json_decode($json, true);
	return $json_output;
}



###########################
## Build the output page ##
###########################

$data = stream_lookup($streamers);
$online = array();

// Gets the total number of online streamers
$output = "<?php \$count = ".count($data).";?>";

// Add the header.php file
$output .= "<?php include('header.php');?>";

// Create a table row for each online streamer
foreach($data as $record){
	$name = $record['channel']['title'];
	array_push($online, array_search($name, $streamers));
	$output .= "<tr>";
	$output .=  "<td><a href='http://twitch.tv/".$name."'>".array_search($name, $streamers)."</a></td>";
	$output .=  "<td>".htmlentities($record['channel']['meta_game'])."</td>";
	$output .=  "<td>".htmlentities($record['title'])."</td>";
	$output .=  "<td>".$record['channel_count']."</td>";
	$output .=  "</tr>";
}


// Build the list of offline streamers to display underneath the table
$str_offline = '';
foreach($streamers as $name => $username){
	if(!in_array($name, $online)){
		$str_offline .= "<a href='http://twitch.tv/".$username."'>".$name."</a>, ";
	}
}

// Add the offline list to the page
$output .= "</table><div class='offline'><h3>Offline:</h3><p>".$str_offline."</p></div>";

// Print the last updated time/date
$output .= "<div class='updated'>Last updated: ".date("jS F Y H:iA")." GMT+2</div>";

// Add footer.php
$output .= "<?php include('footer.php');?>";


###################################
## Write the output to index.php ##
## which will then be displayed  ##
###################################

$file = 'index.php';
file_put_contents($file, $output);
?>

