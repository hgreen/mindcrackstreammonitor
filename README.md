Mindcrack Stream Monitor
=======================

#Description#
A simple PHP script for displaying twitch.tv streaming data for the Mindcrack community.

---

#Usage#
Executing `run_update.php` will send out a new API request to fetch fresh data from twitch.

The information is written to `index.php`, which is displayed to the user. This is a simple caching mechanism to prevent API requests being sent on every page load.
The page reloads automatically every 5 minutes to ensure the user is seeing the most recent data.

It is recommended to set up a CRON job to execute `run_update.php` every 10-15 minutes.

---

#Installation#

* Upload all files to web server in a new (or root) directory:
```
http://yoursite.com/mindcrack
http://localhost/mindcrack
```
* Visit `run_update.php` in your browser to fetch the initial data.
* Set up a CRON job to execute `run_update.php` at your preferred time interval (10-15 minutes is recommended).

---

#Modification#
Adding/removing streamers can be done by simply altering the `$streamers` array at the top of `run_update.php`