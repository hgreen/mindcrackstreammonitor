<div class="copy">Copyright &copy; <?php echo date('Y'); ?> <a href="http://hgreen.co.uk">Harry Green</a> &#124; <a href="https://twitter.com/HJGreen_">@HJGreen_</a> &#124; Distributed under the <a href="http://opensource.org/licenses/mit-license.php">MIT License</a> &#124; <a href="https://bitbucket.org/hgreen/mindcrackstreammonitor">Bitbucket Repository</a></div>
<div class="ad">
	<script type="text/javascript"><!--
		google_ad_client = "ca-pub-8994471748453136";
		/* Mindcrack Stream Monitor */
		google_ad_slot = "5212702518";
		google_ad_width = 728;
		google_ad_height = 90;
		//-->
	</script>
	<script type="text/javascript"
	src="//pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
</div>
</body>
</html>